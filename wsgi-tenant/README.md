# WSGI Tenant Example

In this example, we demonstrate how Instance Separation may be achieved without
creating additional server resources. Instead, the tenants are separated using
different ``wsgi.py`` (and ``settings.py``) files.

## Setup

1) Check out the code.

2) Create a virtual environment:

    cd wsgi-tenant;
    mkvirtualenv -a `pwd` -i requirements.pip --prompt="(Django WSGI Tenant) " django-wsgi-tenant;

3) Create the database and run migrations:

    ./manage.py makemigrations;
    ./manage.py migrate;
    ./manage.py createsuperuser --email=root@ptltd.co --user=root;

> Note: This example uses an SQLite database.

## Example

You can run a management command on the main project:

    ./manage.py help;

You can also run the same command using tenant settings:

    ./manage.py help --settings=example_com.settings;

