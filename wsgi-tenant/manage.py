#!/usr/bin/env python
import os
import sys

# Tenants are added here for working with tenant data.
tenants_path = os.path.join(os.path.abspath("./"), "tenants")
sys.path.append(tenants_path)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "main.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
