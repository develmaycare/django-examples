import os
from random import choice
import string
from fabric.api import abort, lcd, env, local, prefix, task
from jinja2 import Template as JinjaTemplate

env.db_admin = "postgres"
env.db_engine = "postgresql_psycopg2"
env.db_host = "localhost"
env.db_name = None
env.db_pass = None
env.db_port = ""
env.db_user = None
env.operation = None

# Constants

# Helpers


def create_tenant(domain_name):

    # Prepare tenant info.
    domain_tld = domain_name.replace(".", "_")
    tenant_root = "tenants/%s" % domain_tld

    # Database settings.
    db_admin = env.db_admin or "postgres"
    db_host = env.db_host or "localhost"
    db_name = env.db_name or domain_tld
    db_pass = env.db_pass or generate_password()
    db_port = env.db_port or ""
    db_user = env.db_user or domain_tld

    # Do nothing if the tenant already exists.
    if os.path.exists(tenant_root):
        abort("Tenant already exists: %s" % tenant_root)

    # Create the directory and copy the template files.
    local("mkdir %s" % tenant_root)
    local("cp -R templates/tenant_name/* tenants/%s/" % domain_tld)

    # Parse the templates.
    context = {
        'db_host': db_host,
        'db_name': db_name,
        'db_user': db_user,
        'db_pass': db_pass,
        'db_port': db_port,
        'domain_name': domain_name,
        'domain_tld': domain_tld,
        'tenant_root': tenant_root,
    }
    templates = (
        "config/httpd/domain_name.conf",
        "settings.py",
        "wsgi.py"
    )
    for t in templates:
        path = "%s/%s" % (tenant_root, t)
        content = jinja_render(path, context)
        write_file(content, path)

    # Rename the domain_name.conf file.
    with lcd("%s/config/httpd" % tenant_root):
        local("mv domain_name.conf %s.conf" % domain_name)

    # Create the database user.
    command = "createuser -h %(host)s -U %(admin)s -DRS %(name)s" % {
        'admin': db_admin,
        'host': db_host,
        'name': domain_tld,
    }
    local(command)

    sql = "ALTER USER %(name)s WITH ENCRYPTED PASSWORD '%(pass)s'" % {
        'name': db_admin,
        'pass': db_pass,
    }
    command = 'psql -h %(host)s -U %(admin)s -c "%(sql)s"' % {
        'admin': db_admin,
        'host': db_host,
        'name': domain_tld,
        'sql': sql
    }
    local(command)

    # Create the tenant's database.
    command = "createdb -h %(host)s -U %(admin)s -O %(owner)s %(name)s" % {
        'admin': db_admin,
        'host': db_host,
        'name': domain_tld,
        'owner': domain_tld
    }
    local(command)

    # Sync the database. Load fixtures. This should include the root user.
    with prefix("workon django-wsgi-tenant"):
        command = "./manage.py migrate --settings=%s.settings" % domain_tld
        local(command)

        command = "./manage.py loaddata --settings=%s.settings" % domain_tld
        command += " %s/tmp/fixtures.json" % domain_tld
        local(command)


def evict_tenant(domain_name):
    """Remove a tenant."""

    # Prepare tenant info.
    domain_tld = domain_name.replace(".", "_")
    tenant_root = "tenants/%s" % domain_tld

    # Database settings.
    db_admin = env.db_admin or "postgres"
    db_host = env.db_host or "localhost"
    db_name = env.db_name or domain_tld
    # db_port = env.db_port or ""
    db_user = env.db_user or domain_tld

    # Create a backup directory.
    backup_path = "%s/backup" % tenant_root
    if not os.path.exists(backup_path):
        local("mkdir %s" % backup_path)

    # Dump the tenant's database.
    with lcd(backup_path):
        command = "pg_dump -h %(host)s -U %(admin)s -f %(file)s %(name)s" % {
            'admin': db_admin,
            'file': "database.sql",
            'host': db_host,
            'name': domain_tld,
        }
        local(command)

    # Create a backup of the tenant's content files.
    with lcd(tenant_root):
        local("cp -R www/content %s/" % backup_path)

    # Compress the backup and move it to a safe location.
    with lcd(tenant_root):
        local("tar -zvf %s.tgz backup" % domain_name)
        local("mv %s.tgz ../../backups" % domain_name)

    # Remove the tenant directory.
    local("rm -Rf %s" % tenant_root)

    # Remove the database.
    command = "dropdb -h %(host)s -U %(admin)s %(name)s" % {
        'admin': db_admin,
        'host': db_host,
        'name': db_name,
    }
    local(command)

    # Remove the database user.
    command = "dropuser -h %(host)s -U %(admin)s %(name)s" % {
        'admin': db_admin,
        'host': db_host,
        'name': db_user,
    }
    local(command)


def generate_password(length=12, letters=None, numbers=None):
    """Generate a random password.

    :param length: The length of the password.
    :type length: int

    :param letters: The letters to use. Defaults to ``string.letters``.
    :type letters: str

    :param numbers: The numbers to use. Defaults to ``string.digits``.
    :type numbers: str

    :rtype: str

    """
    if not letters:
        letters = string.letters

    if not numbers:
        numbers = string.digits

    characters = letters + numbers

    # noinspection PyUnusedLocal
    return "".join([choice(characters) for i in range(length)])


def jinja_render(template_path, context):
    """Render using Jinja 2.

    :param template_path: Path to the template file.
    :type template_path: str

    :param context: Context variables for the template.
    :type context: dict

    :rtype: str

    """

    # Load the template.
    with open(template_path, "rb") as f:
        template = JinjaTemplate(f.read())
        f.close()

    # Parse the template.
    return template.render(**context)


def list_tenants():
    """List the tenants that are currently installed."""
    local("find tenants/* -type d")


def reload_tenant(domain_name):
    """Perform a soft reload on a tenant."""
    domain_tld = domain_name.replace(".", "_")
    local("touch tenants/%s/wsgi.py" % domain_tld)


def write_file(content, path):
    """Write (replace) the content of a file.

    :param content: The content to be written.
    :type content: str

    :param path: The path to be written.
    :type path: str

    """
    with open(path, "wb") as f:
        f.write(content)
        f.close()


# Tasks


@task()
def db(admin=None, engine=None, host=None, name=None, password=None, port=None,
       user=None):
    """Set database parameters."""

    if admin:
        env.db_admin = admin

    if engine:
        env.db_engine = engine

    if host:
        env.db_host = host

    if name:
        env.db_name = name

    if password:
        env.db_password = password

    if port:
        env.db_port = port

    if user:
        env.db_user = user


@task()
def evict():
    """Prepare an eviction (uninstall) operation."""
    env.operation = "evict"


@task()
def listing():
    """Request a listing."""
    env.operation = "listing"
    list_tenants()


@task()
def new():
    """Flag the operation as new."""
    env.operation = "new"


@task()
def reloading():
    """Flag a reload operation."""
    env.operation = "reload"


@task()
def tenant(domain_name):
    """Work with tenants."""

    if env.operation == "evict":
        evict_tenant(domain_name)
    elif env.operation == "new":
        create_tenant(domain_name)
    elif env.operation == "reload":
        reload_tenant(domain_name)
    elif env.operation == "update":
        pass
    else:
        abort("Invalid tenant operation: %s" % env.operation)


@task()
def update():
    """Prepare an update operation."""
    env.operation = "update"
