#! /bin/bash

###################################
# Functions
###################################

# We need to make sure that everything runs relative to the same directory as
# this script.
SCRIPT_PATH="`readlink -f $0 | xargs dirname`";
cd $SCRIPT_PATH;

# Load functions.
if [[ ! -f "$MYNINJAS_SYSTEM_ROOT/lib/functions.sh" ]]; then
    echo "MyNinjas functions could not be loaded -- is it installed?";
    exit 3;
fi;
source "$MYNINJAS_SYSTEM_ROOT/lib/functions.sh";

###################################
# Configuration
###################################

# Script information.
AUTHOR="F.S. Davis <consulting@fsdavis.com>";
SCRIPT=`basename $0`;
DATE="2013-01-15";
VERSION="0.3.0-d";

# Temporary file.
TEMP_FILE="/tmp/$SCRIPT".$$

# Load constants.
if [[ ! -f ../workflow/constants.sh ]]; then
    echo "../workflow/constants.sh file not found.";
    quit $EXIT_ENVIRONMENT;
fi;
source ../workflow/constants.sh;

# Load the local environment.
if [[ ! -f ../environment.sh ]]; then
    echo "environment.sh file not found.";
    quit $EXIT_ENVIRONMENT;
fi;
source ../environment.sh;

###################################
# Help
###################################

HELP="
SUMMARY

Create a new Agtronics SaaS tenant.

REQUIRED

-A <apps>
    Comma separated list of apps to be enabled for this tenant. Choices are:

        bin_sentry
        dryer_sentry
        feed_sentry
        water_sentry

-d domain
    The domain to use for the tenant. The typical use is to supply
    a subdomain that will be served from ``example.agtronics.us``, however, it
    is possible to run the tenant as ``example.com`` provided you have access
    to DNS information for the given domain name.

OPTIONS

-D <environment>
    Load sample data for the specified environment, which should be development,
    testing, demo or production. Defaults to $ENVIRONMENT.


-E
    Load extra app data. See NOTES under ../workflow/install/6_database.sh

-h
    Print help and exit.

--help
    Print more help and exit.

-V
    Verbose output.

-v
    Print version and exit.

--version
    Print full version and exit.

";

# Help and information.
if [[ $1 = '--help' ]]; then echo "$HELP"; quit; fi;
if [[ $1 = '--version' ]]; then echo "$SCRIPT $VERSION ($DATE)"; quit; fi;

###################################
# Arguments
###################################

load_extra_data="f";
load_sample_data="f";
sample_data_environment=$ENVIRONMENT;
verbose="f";

while getopts "A:D:d:EhVv" arg
do
    case $arg in
        A) app_list=$OPTARG;;
        D)
            sample_data_environment=$OPTARG;
            load_sample_data="t";
        ;;
        d) domain_name=$OPTARG;;
        E) load_extra_data="t";;
        V) verbose="t";;
        v) echo "$VERSION"; quit;;
        h) echo "$SCRIPT <REQUIRED> [OPTIONS]"; quit;;
        *) echo "Unsupported option: $arg"; quit;;
    esac
done

# Make sure we have all the required arguments.
if [[ -z "$domain_name" ]]; then
    echo "Domain name is required. Try $SCRIPT --help";
    quit $EXIT_USAGE;
fi;
domain_tld=`echo $domain_name | sed 's/\./_/g'`;

tenant_root="/domains/$domain_tld";
if [[ -d $tenant_root ]]; then
    echo "$domain_name tenant already exists.";
    quit $EXIT_ENVIRONMENT;
fi;

if [[ -z "$app_list" ]]; then
    echo "App list is required. Try $SCRIPT --help";
    quit $EXIT_USAGE;
fi;

###################################
# Procedure
###################################

# Update exit code below to use as the final exit.
exit_code=0;

# Create the site directory.
feedback "Creating tenant directory structure ...";
mkdir "/domains/$domain_tld";

# Copy skeleton files and directories.
feedback_verbose "Creating tenant files ...";
cp -R ../workflow/install/templates/tenant/* /domains/$domain_tld/;

# For now, we are setting a generic database password.
db_name="$domain_tld";
db_pass="4gtr0nic5";
db_user="$domain_tld";

# Create a python string for the installed apps. Also define which fixture
# directories to check.
controlbyweb_required="f";
fixture_dirs="apps/contacts apps/facilities";
installed_apps="";
port_monitor_required="f";
for app in `echo $app_list | sed "s/,/ /g"`
do
    case $app in
        bin_sentry)
            controlbyweb_required="t";
            fixture_dirs="$fixture_dirs contrib/controlbyweb/django/control_by_web_devices apps/bin_sentry contrib/port_monitors";
            installed_apps="'bin_sentry', 'bin_sentry.cbw_bs', 'bin_sentry.grain_bins', 'bin_sentry.load_outs', $installed_apps";
            port_monitor_required="t";
        ;;
        feed_sentry)
            fixture_dirs="$fixture_dirs apps/feed_sentry";
            installed_apps="'feed_sentry', 'feed_sentry.feed_bins', 'feed_sentry.feed_level_sensors', $installed_apps";
        ;;
        *)
            fixture_dirs="$fixture_dirs $app";
            installed_apps="'$app', $installed_apps";
        ;;
    esac
done
if [[ $controlbyweb_required == "t" ]]; then
    installed_apps="'controlbyweb.django.control_by_web_devices', 'controlbyweb.django.five_input_modules', 'controlbyweb.django.web_relay_boards', $installed_apps";
fi;
if [[ $port_monitor_required == "t" ]]; then
    installed_apps="'port_monitors', $installed_apps";
fi;

# We need the python directory name for parsing templates.
python_version=`ls -d --color=never ../python/lib/python* | xargs basename`;

# The SYSTEM_ROOT must be escaped for search/replace activities below.
escaped_system_root=`echo $SYSTEM_ROOT | sed "s,/,\\\/,g"`;

# The tenant root must be escaped for search/replace activities below.
escaped_tenant_root=`echo $tenant_root | sed "s,/,\\\/,g"`;

# Save context variables to a file to make them easier to use and manage.
echo "s/{{ APP_LIST }}/$installed_apps/g" > $TEMP_FILE;
echo "s/{{ DB_ENGINE }}/postgresql_psycopg2/g" >> $TEMP_FILE;
echo "s/{{ DB_HOST }}/$DB_HOST/g" >> $TEMP_FILE;
echo "s/{{ DB_NAME }}/$db_name/g" >> $TEMP_FILE;
echo "s/{{ DB_PASS }}/$db_pass/g" >> $TEMP_FILE;
echo "s/{{ DB_PORT }}//g" >> $TEMP_FILE;
echo "s/{{ DB_USER }}/$db_user/g" >> $TEMP_FILE;
echo "s/{{ DOMAIN_NAME }}/$domain_name/g" >> $TEMP_FILE;
echo "s/{{ DOMAIN_TLD }}/$domain_tld/g" >> $TEMP_FILE;
echo "s/{{ ENVIRONMENT }}/$ENVIRONMENT/g" >> $TEMP_FILE;
echo "s/{{ PYTHON_VERSION }}/$python_version/g" >> $TEMP_FILE;
echo "s,{{ SYSTEM_ROOT }},$escaped_system_root,g" >> $TEMP_FILE;
echo "s,{{ TENANT_ROOT }},$escaped_tenant_root,g" >> $TEMP_FILE;

# Parse files for context variables.
feedback_verbose "Parsing template files ...";
for f in `find /domains/$domain_tld -type f`
do
    sed -i -f $TEMP_FILE $f;
done

# Rename conf files.
feedback_verbose "Renaming domain_name.conf file to $domain_name.conf ...";
(cd /domains/$domain_tld/config/httpd; mv domain_name.conf $domain_name.conf);

# Create a symlink for the tenant directory.
feedback_verbose "Creating symlink to tenant's Django files ...";
(cd $SYSTEM_ROOT/tenants; ln -s /domains/$domain_tld/agtronics_tenant $domain_tld);

# Create database user with no privileges.
feedback_verbose "Creating database user ...";
createuser -h $DB_HOST -U $DB_USER -DRS $db_user;
psql -h $DB_HOST -U $DB_USER -c "ALTER USER $db_user WITH ENCRYPTED PASSWORD '$db_pass'";

# Create the database.
feedback "Creating database ...";
createdb -h $DB_HOST -U $DB_USER -O $db_user $db_name;

# Run syncdb. Without the --noinput option, the user will be prompted to create
# the root user, which is okay for now.
feedback_verbose "Uploading database schema ...";
cd $SYSTEM_ROOT/main;
#python manage.py syncdb --noinput --settings=$domain_tld.settings;
python manage.py syncdb --settings=$domain_tld.settings;

# Load sample data as needed.
if [[ $load_sample_data == "t" ]]; then
    feedback_verbose "Preparing to load fixture data ...";
    feedback_verbose "$fixture_dirs";
    cd $SYSTEM_ROOT/main;
    for d in $fixture_dirs
    do
        fixture_path="$d/fixtures/$sample_data_environment""_data.json";
        if [[ -f $fixture_path ]]; then
            feedback_verbose "Loading fixtures from $fixture_path ...";
            python manage.py loaddata --settings=$domain_tld.settings $fixture_path;
        fi;
    done
fi;

# Load extra data as needed.
if [[ $load_extra_data == "t" ]]; then
    feedback "Preparing to load extra data ...";
    export PGPASSWORD=$db_pass;
    for a in bin_sentry dryer_sentry feed_sentry water_sentry
    do
        extra_data_path="$SYSTEM_ROOT/main/apps/$a/fixtures/extra";
        if [[ -d $extra_data_path ]]; then
            for i in $extra_data_path/*.csv
            do

                # The table name is derived from the file name.
                file_name=`basename $i`;
                order=`echo "$file_name" | awk -F "_" '{print $1}'`; # echo $order;
                table_name=`echo "$file_name" | sed "s/$order\_//g" | awk -F "." '{print $1}'`; # echo $table_name;

                # Postgres still seems to require the columns listed, even if
                # the HEADER argument is given.
                columns=`head -1 $i`; #echo $columns;
                query="\copy $table_name ($columns) FROM '$i' DELIMITER ',' CSV HEADER"; # echo "$query";
                command=`psql_command "$db_host" "$db_port" "$db_name" "$db_user" "$db_pass" "-f $TEMP_FILE" ""`;
                echo "$command";

                # Query line length may exceed that supported by the shell.
                echo "$query" > $TEMP_FILE;
                feedback_verbose "Loading data from $i";
                $command;

            done
        fi;
    done
fi;


# TODO: Create an admin user.

# Log the activity.
logger -i -t $SCRIPT "$SUDO_USER created agtronics tenant $domain_name";

# Clean up and exit.
quit $exit_code;

# vim: set foldmethod=marker:
