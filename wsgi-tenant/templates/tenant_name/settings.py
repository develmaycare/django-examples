# Import default settings. Override or add to these settings below.
# noinspection PyUnresolvedReferences
from main.settings import *

# Tenant root. The directory in which the all tenant files are stored.
TENANT_ROOT = "{{ tenant_root }}"

# Database settings. Each tenant gets their own database.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.{{ db_engine }}', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': '{{ db_name }}',                      # Or path to database file if using sqlite3.
        'USER': '{{ db_user }}',                      # Not used with sqlite3.
        'PASSWORD': '{{ db_pass }}',                  # Not used with sqlite3.
        'HOST': '{{ db_host }}',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '{{ db_port }}',                      # Set to empty string for default. Not used with sqlite3.
    }
}

# Installed apps. Control which apps are available to the tenant.
# INSTALLED_APPS += (
#     {{ APP_LIST }}
# )

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = '%s/www/content' % TENANT_ROOT

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/content/'
