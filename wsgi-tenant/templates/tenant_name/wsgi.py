"""
WSGI config for a tenant of the Agtronics SaaS project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import site
import sys
from django.core.wsgi import get_wsgi_application

# System Setup
# sys.path.insert(0, "{{ SYSTEM_ROOT }}/python/lib/{{ PYTHON_VERSION }}/site-packages/")
# site.addsitedir("{{ SYSTEM_ROOT }}/python/lib/{{ PYTHON_VERSION }}/site-packages/")
# sys.path.insert(1, "{{ SYSTEM_ROOT }}/main")
# sys.path.insert(2, "{{ SYSTEM_ROOT }}/main/apps")
# sys.path.insert(3, "{{ SYSTEM_ROOT }}/main/contrib")
#
# Tenant Setup
# os.environ['DJANGO_SETTINGS_MODULE'] = "agtronics_tenant.settings"
# os.environ['PYTHON_EGG_CACHE'] = "{{ TENANT_ROOT }}/tmp/egg_cache"
sys.path.insert(4, "{{ tenant_root }}")

# Django Setup

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{ domain_tld }}.settings")

application = get_wsgi_application()
