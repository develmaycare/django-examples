# Django Examples

## Setup

1) Create the examples directory if it does not already exist:

    mkdir -p $PROJECT_HOME/examples;

2) Check out the code:

    cd $PROJECT_HOME/examples/;
    git clone git@bitbucket.org:develmaycare/django-examples.git django;

3) Go to the example you wish to try and finish the setup there.

